#include "los_hwi.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define TIMER_ENABLE_BIT           7
#define TIMER_COUNTING_MODE_BIT    6
#define TIMER_INTERRUPT_ENABLE_BIT 5
#define TIMER_SIZE_SELECT_BIT      1
#define TIMER_ONESHOT_BIT          0

VOID HrtimerClockIrqClear(VOID)
{
    WRITE_UINT32(BIT(0), 0x01c60004);//clear timer 2
}

VOID HrtimerClockInit(VOID)
{
    UINT32 temp;
	
    READ_UINT32(temp, 0x01c202c0);
    temp |= BIT(19);//asset ahb reset
    WRITE_UINT32(temp, 0x01c202c0);

    /* enable timer here */
    READ_UINT32(temp, 0x01c20060);
    temp |= BIT(19);//enable ahb gating
    WRITE_UINT32(temp, 0x01c20060);

    /* disable timer */
	WRITE_UINT32(0x0, 0x01c60010);
    WRITE_UINT32(0x0, 0x01c60000);

    /*
     * Timing mode:oneshot [bit 0 set as 1]
     * timersize:32bits [bit 1 set as 1]
     * ticking with 1/1 clock frequency [bit 3 set as 0, bit 2 set as 0]
     * interrupt enabled [bit 5 set as 1]
     * timing circulary [bit 6 set as 1]
     */
    temp = BIT(7) | (2 << 4);
    WRITE_UINT32(temp, 0x01c60010);
}

VOID HrtimerClockStart(UINT32 period)
{
    UINT64 temp;
	UINT32 reg;
	
	temp = period * 6;

    reg = (UINT32)temp;

    /* set init value as period */
    WRITE_UINT32(reg, 0x01c60014);
	
    reg = (UINT32)(temp >> 32);
	WRITE_UINT32(reg, 0x01c60018);

    READ_UINT32(reg, 0x01c60010);
    /* timer enabled [bit 7 set as 1] */
    reg |= BIT(0);
    WRITE_UINT32(reg, 0x01c60010);
	//enable interrupt
	READ_UINT32(reg, 0x01c60000);
	reg |= BIT(0);
	WRITE_UINT32(reg, 0x01c60000);
	
}

VOID HrtimerClockStop(VOID)
{
    UINT32 reg;
    READ_UINT32(reg, 0x01c60010);
    /* timer enabled [bit 7 set as 1] */
    reg &= ~BIT(0);
    WRITE_UINT32(reg, 0x01c60010);
	//enable interrupt
	READ_UINT32(reg, 0x01c60000);
	reg &= ~BIT(0);
	WRITE_UINT32(reg, 0x01c60000);
}

UINT32 HrtimerClockValueGet(VOID)
{
    UINT32 temp1, temp2;
    UINT64 temp;

    /* Read the current value of the timer3 */
    READ_UINT32(temp1, 0x01c60014);
	READ_UINT32(temp2, 0x01c60018);

    temp = temp1 | ((UINT64)temp2 << 32);
    return (UINT32)temp;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */
